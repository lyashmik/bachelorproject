\renewcommand{\arraystretch}{2}
\chapter{Background and State-of-the-Art}
\label{chap.stateoftheart}
In this chapter two main parts are mentioned. In theoretical background is gathered all the information needed to understand the subject of the work. In Previous Results and Related Work will be mentioned all the articles, which helped in writing of this work.

\section{Theoretical Background}
\subsection{Artificial Neural Networks}
Time-series data is a consecutive collection of observation made in time. Order flow in this work is meant as an overall information in the market, meaning both limit order books and executed transactions. Limit order book is a collection of buy and sell orders on different levels, meaning the highest buy order is on the top of buy orders and the lowest sell order is on the bottom of sell orders. Limit order book is update every tick, which is the measure of minimum upward or downward movement in the price of an asset. Limit order books are believed to be the makers of the price, because the price lays between the highest buy order and the lowest sell order.\\
Recurrent neural network (RNN) \cite{deepl} is a class of artificial neural networks, which characteristic feature is the transfer of signals from the output layer or hidden layer to the input layer. Basic architecture of an RNN can be seen on the Figure \ref{fig.RNN}. 

\begin{figure}[htb]
\begin{center}
\includegraphics[width=.95\textwidth]{pic/RNN.png}
\end{center}
\caption{Basic architecture of RNN.}
\label{fig.RNN}
\end{figure}
\newpage
RNN is commonly used for time series predictions. Backward propagation of errors is used to train the model, it defines a strategy for selecting neural network's weights using gradient optimization methods. The task is to solve the problem of minimizing total losses on all the training samples:
\begin{equation}
    Q(W) = \sum_{t=0}^{n-1}L_t(W) \xrightarrow{} \min_W,
\end{equation}
where:
\begin{itemize}
    \item $W$ - weight vector of a neural network
    \item $n(n \in \mathbb N_0)$ - number of training samples
    \item $L_t(W)$ - loss function on sample $x_t$ of index $t$
\end{itemize}

On each step $t$ of learning neural network is given an input vector $x_t$, for which an output $y_t$ is expected. The weights for neurons of the hidden layer (or hidden layers) are calculated for the input vector $x_t$. Output vector $k_t$($k_t \in R^n$) of the hidden layer is calculated like this on each step $t$:
\begin{equation}
    k_t = \sigma_k\*(W_x\cdot x_t+W_k\cdot k_{t-1}+b_k)
\end{equation}
where:

\begin{itemize}
    \item $x_t$($x_t \in \mathbb R^L$) is an input vector on each step $t$,
    \item $W_x$($W_x \in \mathbb R^{L\times K}$) is a matrix of weights for the input vector,
    \item $W_y$($W_y \in \mathbb R^{K\times K}$) is a matrix of weights for the hidden layer,
    \item $b_k$($b_k \in \mathbb R^K$) is a shift vector,
    \item $\sigma_k$ is an activation function for the hidden layer.
\end{itemize}
\newpage
There are four commonly used activation functions: Sigmoid, Tanh, ReLU and ReLU alternative, called LeakyReLU, which are depicted in Table \ref{tab:table-name}.
\begin{table}
    \begin{center}
    \begin{tabular}{|c|c|c|}
        \hline \textbf{Function name} & \textbf{Function} & \textbf{Value Range}\\
        \hline Sigmoid & $g(z) = \frac{1}{1+e^{-z}}$ & $(0, 1)$\\
        \hline Tanh & $g(z) = \frac{e^z - e^{-z}}{e^z + e^{-z}}$ & $(-1, 1)$\\
        \hline ReLU & $g(z) = max(0, z)$ & $(0, +\infty)$\\
        \hline LeakyReLU & $g(z) = max(0.1\cdot z, z)$ & $(-\infty,+\infty)$\\
        \hline
    \end{tabular}
    \end{center}
    \caption{\label{tab:table-name}Table of activation functions.}
\end{table}
The memory implemented in RNN network is short, because at each learning step information in the memory is mixed with new information, that way it is completely overwritten after a few steps.

Long Short-Term Memory Recurrent Neural Network (LSTM-RNN) is a particular type of a recurrent neural network. However, simple recurrent neural networks can only account for the network's recent past states. This problem is called the problem of long-run dependence or the vanishing gradient problem. In theory, this problem can be solved by choosing the right network parameters, but the problem of choosing these parameters is still unsolved. LSTM networks are designed to solve that kind of problem and allow to detect both long and short data patterns and partially eliminate the problem of vanishing gradient. LSTM architecture in extended view can be seen on the Figure \ref{fig.LSTM} \cite{gers2000learning}.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=.95\textwidth]{pic/LSTM.png}
\end{center}
\caption{LSTM architecture, including cell view.}
\label{fig.LSTM}
\end{figure}

\newpage

LSTM cell works like this:
\begin{enumerate}
    \item Lower horizontal line on Figure \ref{fig.LSTM} given as an input for a cell represents previous hidden state $h_{t-1}$. It proceeds as an input for a sigmoid function, that works as forget filter. Output is given as one of the values: 0 or 1, where 0 means \textquote{forget}. This part of LSTM cell is called a \textbf{Forget Gate}. The equation for \textbf{Forger Gate} is:
    \begin{equation}
        f_t = \sigma(W_f x_t + U_f h_{t-1} + b_f),
    \end{equation}
    where:
    \begin{itemize}
        \item $t$ - timestep
        \item $f_t$($f_t \in \mathbb R^K$) - \textbf{Forget Gate} at t
        \item $x_t$($x_t \in \mathbb R^L$) - input vector
        \item $h_{t-1}$($h_{t-1} \in \mathbb R^K$) - previous hidden state
        \item $W_f$($W_f \in \mathbb R^{K\times L}$) - Weight matrix for input gate $x_t$
        \item $U_f$($U_f \in \mathbb R^{K\times K}$) - Weight matrix for previous hidden state $h_{t-1}$
        \item $b_f$($b_f \in \mathbb R^K$) - connection bias
    \end{itemize}
    \item Next sigmoid function, which gets $x_t$ as an input, decides what values should be changed. Tanh function makes a vector of new values, that could be added to a new cell state $C_t$, which is represented by the upper horizontal line on Figure \ref{fig.LSTM}. This part of LSTM cell is called an \textbf{Input Gate}. It produces two values:
    \begin{enumerate}
        \item 
            \begin{equation}
                i_t = \sigma(W_i x_t + U_i h_{t-1} + b_i),
            \end{equation}
        \item
            \begin{equation}
                \Tilde{C_t} = tanh(W_C x_t + U_C h_{t-1} + b_C),
            \end{equation}
    \end{enumerate}
    where:
    \begin{itemize}
        \item $t$ - timestep
        \item $i_t$($i_t \in \mathbb R^K$) - \textbf{Input Gate} at t
        \item $W_i$($W_i \in \mathbb R^{L\times K}$) and $W_C$($W_C \in \mathbb R^{L\times K}$) - weight matrices for input vector $x_t$
        \item $U_i$($U_i \in \mathbb R^{K\times K}$) and $U_C$($U_C \in \mathbb R^{K\times K}$)- weight matrices for previous hidden state $h_{t-1}$
        \item $\Tilde{C_t}$ - value generated by tanh
        \item $b_i$($b_i \in \mathbb R^K$) and $b_C$($b_C \in \mathbb R^K$) - connection biases
    \end{itemize}
    \item To update the old state the output from the \textbf{Forget Gate} $f_t$($f_t \in \mathbb R^K$) is multiplied with the the old state $C_{t-1}$($C_{t-1} \in \mathbb R^K$) and then its added with the multiplies outputs from the \textquote{Input Gate}: $i_t$($i_t \in \mathbb R^K$) and $\Tilde{C_t}$($\Tilde{C_t} \in \mathbb R^K$). This produces a new LSTM state $C_t$($C_t \in \mathbb R^K$).
    \begin{equation}
        C_t = f_t*C_{t-1} + i_t*\Tilde{C_t}
    \end{equation}
    \item Output from the previous LSTM cell together with the input $x_t$ goes to sigmoid function and gets multiplied with the tanh function of the new state, which produces the output of the current LSTM cell. This part of the cell is called the \textbf{Output Gate}. In equations:
    \begin{enumerate}
        \item 
            \begin{equation}
                o_t = \sigma(W_o x_t + U_o h_{t-1} + b_o),
            \end{equation}
        \item
            \begin{equation}
                h_t = o_t*tanh(C_t),
            \end{equation}
    \end{enumerate}
    where:
    \begin{itemize}
        \item $t$ - timestep
        \item $o_t$($o_t \in \mathbb R^K$) - \textbf{Output Gate} at $t$
        \item $W_o$($W_i \in \mathbb R^{K\times L}$) - weight matrix for input vector $x_t$
        \item $U_o$($U_o \in \mathbb R^{K\times K}$) - weight matrix for previous hidden state $h_{t-1}$
        \item $b_o$($b_o \in \mathbb R^K$) - connection biases
        \item $C_t$($C_t \in \mathbb R^K$) - new cell state
    \end{itemize}
\end{enumerate}
\newpage
Gate recurrent network, in comparison with LSTM, does not have states and consists of only two gates: \textbf{Reset Gate} and \textbf{Update Gate}. The cell architure of GRU is given on the Figure \ref{fig.GRU}.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=.7\textwidth]{pic/GRU.png}
\end{center}
\caption{GRU cell architecture.}
\label{fig.GRU}
\end{figure}

GRU cell works like this:
\begin{enumerate}
    \item Input vector together with the output vector from the previous cell is fed to sigmoid function. This is the result of the \textbf{Update Gate}. In equation:
    \begin{equation}
        z_t = \sigma(W_z x_t + U_z h_{t-1} + b_z),
    \end{equation}
    where:
    \begin{itemize}
        \item $t$ - timestep
        \item $z_t$($z_t \in \mathbb R^K$) - \textbf{Update Gate} at $t$
        \item $x_t$($x_t \in \mathbb R^L$) - input vector
        \item $h_{t-1}$($h_{t-1} \in \mathbb R^K$) - output vector from previous cell
        \item $W_z$($W_z \in \mathbb R^{K\times L}$) - weight matrix for input vector $x_t$
        \item $U_z$($U_z \in \mathbb R^{K\times K}$ - weight matrix for output vector from previous cell $h_{t-1}$
        \item $b_z$($b_z \in \mathbb R^K$ - connection bias
    \end{itemize}
    
    \newpage
    \item Similar to the previous step is made in the \textbf{Reset Gate}.
    \begin{equation}
        r_t = \sigma(W_r x_t + U_r h_{t-1} + b_r),
    \end{equation}
    where:
    \begin{enumerate}
        \item $r_t$($r_t \in \mathbb R^K$) - \textbf{Reset Gate} at $t$
        \item $W_r$($W_r \in \mathbb R^{K\times L}$) and $U_r$($U_r \in \mathbb R^{K\times K}$) - weight matrices
        \item $b_r$ - connection bias
    \end{enumerate}
    \item Then an intermediate value $\Tilde{h_t}$ of output vector is counted, which works as a decision maker in term of what information is to be discarded from the previous step.
    \begin{equation}
        \Tilde{h_t} = tanh(W_h x_t + r_t \odot U_h h_{t-1} + b_h),
    \end{equation}
    where:
    \begin{itemize}
        \item $\Tilde{h_t}$($h_t \in \mathbb R^K$) - intermediate value of output vector
        \item $W_h$($W_h \in \mathbb R^{K\times L}$) and $U_h$($U_h \in \mathbb R^{K\times K}$) - weight matrices
        \item $b_h$ - connection bias
    \end{itemize}
    \item Output vector $h_t$($h_t \in \mathbb R^K$) is counted using intermediate value of output vector and output of the \textbf{Update Gate}:
    \begin{equation}
        h_t = z_t \odot h_{t-1} + (1-z_t) \odot \Tilde{h_t},
    \end{equation}
\end{enumerate}

Deep neural networks (DNN) can be used both for regression and classification problems. Regression problem is the the problem, when linear value is expected as an output and classification problem is when a set of predetermined values is expected. 
\newpage
\subsection{Optimizer}
Adaptive Momentum Estimation (Adam) \cite{kingma2014adam} is going to be used in a final stage in a training of the models. It is an optimization algorithm for gradient descent. In contrast to the classical stochastic gradient decent, it does not maintain a single learning rate during training, but adapts. Adam algorithm can be described as a combination of Adaptive Gradient Algorithm (AdaGrad) \cite{duchi2011adaptive} and Root Mean Square Propagation (RMSProp) \cite{hinton2012lecture}. In RMSProp learning rates are adapted based upon the first moment (mean), whereas in Adam the second moment of the gradients (the uncentered variance) is also used. The formulas used in Adam optimizer are:
\begin{enumerate}
    \item \begin{equation}
    m_t = \beta_1 m_{t-1} + (1 - \beta_1)g_t,
        \end{equation}
    \item \begin{equation}
        v_t = \beta_2 v_{t-1} + (1 - \beta_2)g_t^{2},
    \end{equation}
\end{enumerate}
where:
\begin{itemize}
    \item $m_t$ - moment, which helps in faster convergence of the loss function
    \item $v_t$ - second moment
    \item $\beta_1$ and $\beta_2$ - decay rates
    \item $g_t$ - gradient function
\end{itemize}
%\newpage
Usually $m_t$ and $v_t$ are initialized by zero vectors. To solve this kind of a problem following upgrades are made:
\begin{enumerate}
    \item 
    \begin{equation}
            \Tilde{m_t} = \frac{m_t}{1 - \beta_1},
    \end{equation}
    \item
    \begin{equation}
        \Tilde{v_t} = \frac{v_t}{1 - \beta_2}
    \end{equation}
\end{enumerate}
The final parameter update formula look like this:
\begin{equation}
    \theta_{t+1} = \theta_t - \frac{\mu}{\sqrt{\Tilde{v_t}} + \epsilon}\Tilde{m_t},
\end{equation}
where:
\begin{itemize}
    \item $\mu$ - learning rate
    \item $\epsilon$ - a small constant to avoid division by 0
\end{itemize}

\subsection{Stationarity}
Time-series data can be stationary or non-stationary. Members of non-stationary data are dependent on each other, meaning that the trend can be extracted from the data. To check the data stationarity Augmented Dickey Fuller (ADF) \cite{cheung1995lag} test is used. To understand ADF, firstly unit root test should be observed. Having an autoregressive model:
\begin{equation}
    y_t = \sum_{i=0}^{n-1}a_i y_{t-i} + \varepsilon_t
\end{equation}


If all $a_i$ are equal to 1, then the autoregressive process is considered to be non-stationary. Otherwise if each $|a_i|$ is less, than 1, then the autoregressive process is stationary.\\
The autoregressive equation given before can be written like this:
\begin{equation}
    \Delta y_t = \delta y_{t-1} + \varepsilon_t,
\end{equation}
where:
\begin{itemize}
    \item $\delta = a - 1$
    \item $\lambda y_t = y_t - y_{t-1}$
\end{itemize}
%\newpage
Three versions of a Dickey-Fuller test exist:
\begin{enumerate}
    \item Without a constant and a trend:
    \begin{equation}
        \Delta y_t = \delta y_{t-1} + \varepsilon_t
    \end{equation}
    \item With constant, without trend:
    \begin{equation}
        \Delta y_t = \alpha + \delta y_{t-1} + \varepsilon_t
    \end{equation}
    \item With constant and linear trend:
    \begin{equation}
        \Delta y_t = \alpha + \gamma t + \delta y_{t-1} + \varepsilon_t
    \end{equation}
\end{enumerate}
The null hypothesis stays the same: if $b$ is equal to 0, then it refers to non-stationarity. For each of the tested regressions corresponding critical values of DF-statistic exist. If DF-statistic lies to the left of the critical value (critical values are negative), then null hypothesis is rejected and and the process is considered stationary.\\
Augmented Dickey-Fuller test is a slight alternation of classic Dickey-Fuller test, one more term is included $\beta \Delta y_{t-1}$. So the equation with constant and without trend would look like this:
\begin{equation}
    \Delta y_t = \alpha + \delta y_{t-1} + \beta y_{t-1} + \varepsilon_t
\end{equation}

\section{Previous Results and Related Work}
One of the RNNs applications is time series prediction. There are works, that try to predict future cryptocurrency's price, based on the price itself\cite{BitcoinPricePrediction}. Despite the fact, that some of this works get pretty solid results, this method is not going to be used in this thesis, because studies \cite{OrderBooks} show, that there is a significant amount of information stored in Limit Order Books. In the article \cite{LSTM} limit order books are used for prediction of stock price movements. LSTM gets a solid F1-score\footnote{F1-score is a harmonic mean of the precision and recall.}, which fluctuates between 61 and 66 percent, depending on the value of prediction horizon. In the course of the work, time series data are going to be used. In the book \cite{stationarity} the author says:\\
\textit{"If we fit a stationary model to data, we assume our data are a realization of a stationary process. So our first step in an analysis should be to check whether there is any evidence of a trend or seasonal effects and, if there is, remove them."}\\
That is why all time series are going to be checked on stationarity and if they are not stationary a method from the book "Forecasting: principles and practice" \cite{differencing} called differencing is going to be applied on the dataset to reduce trend and seasonality. Before feeding the input to the neural network it is strongly advised to scale the data. The author of "Neural Networks for Pattern Recognition" \cite{bishop1995neural} says:\\
\textit{"In practice it is nearly always advantageous to apply pre-processing transformations to the input data before it is presented to a network. Similarly, the outputs of the network are often post-processed to give the required output values."}\\
That is why normalization is applied to input and output.
During training the neural networks some regularization techniques are used. In the journal paper "Dropout: A Simple Way to Prevent Neural Networks from Overfitting"\cite{dropout} the author describes how much of an improvement gave the Dropout method, during which some nodes are dropped. As another regularization method Batch normalization is used, in the book "Deep Learning" \cite{deepl} the author emphasizes, that it \textit{"can have a dramatic effect on optimization performance"}. In the deep learning models used further in the work RuLU activation functions are used, as the author of a previously mentioned book \cite{deepl} highlights the fact, that in modern neural networks it is recommended to use ReLU as an activation function.