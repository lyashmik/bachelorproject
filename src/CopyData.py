import sqlite3


def copyData():
    conn = sqlite3.connect('DataCopy02.sqlite')
    cur = conn.cursor()

    cur.execute('''ATTACH DATABASE '/home/mike/uceba/diplom_git/diplom/DataCopy.sqlite' AS new_db;''')

    cur.execute('CREATE TABLE new_db.trades (id INTEGER, price FLOAT, qty FLOAT, quoteQty FLOAT, time INTEGER,'
                'isBuyerMaker VARCHAR, isBestMatch VARCHAR, symbol VARCHAR)')
    conn.commit()
    cur.execute('CREATE TABLE new_db.order_books (lastUpdateId INTEGER, price FLOAT, VELOCITY FLOAT, side VARCHAR,'
                'time INTEGER, symbol VARCHAR)')
    cur.execute('CREATE TABLE new_db.assets_prices (price FLOAT, symbol VARCHAR, time INTEGER)')

    cur.execute('INSERT INTO new_db.trades SELECT * FROM main.trades;')
    cur.execute('INSERT INTO new_db.order_books SELECT * FROM main.order_books;')
    cur.execute('INSERT INTO new_db.assets_prices SELECT * FROM main.assets_prices;')

    conn.commit()
    conn.close()


if __name__ == '__main__':
    copyData()