import requests
import time
import psycopg2
import threading

TIME_GAP = 30.0


def create_DB():
    conn = psycopg2.connect(
        host='localhost',
        database='maindb5',
        user='lyashmik',
        password='postgres')
    cur = conn.cursor()

    # get the count of tables with the name
    cur.execute("select exists(select * from information_schema.tables where table_name=%s)", ('trades',))

    # if the count is 1, then table exists
    if cur.fetchone()[0]:
        return

    cur.execute('CREATE TABLE trades (id BIGINT, price FLOAT, qty FLOAT, quoteQty FLOAT, time BIGINT,'
                'isBuyerMaker VARCHAR, isBestMatch VARCHAR, symbol VARCHAR)')
    conn.commit()
    cur.execute('CREATE TABLE assets_prices (price FLOAT, symbol VARCHAR, time BIGINT)')
    conn.commit()
    conn.close()


def insert_symbol(symbol):
    conn = psycopg2.connect(
        host='localhost',
        database='maindb5',
        user='lyashmik',
        password='postgres')

    time_r = time.time()
    r = requests.get("https://api.binance.com/api/v3/ticker/price",
                     params=dict(symbol=symbol))
    prices = r.json()
    r = requests.get("https://api.binance.com/api/v3/trades",
                     params=dict(symbol=symbol))
    trades = r.json()

    cur = conn.cursor()
    for trade in trades:
        trade['symbol'] = symbol
        sql = """insert into trades(id, price, qty, quoteQty, time, isBuyerMaker, isBestMatch, symbol) values(%s, %s, %s, %s, %s, %s, %s, %s)"""
        cur.execute(sql,
                    [trade['id'], float(trade['price']), float(trade['qty']), float(trade['quoteQty']),
                     int(trade['time']), trade['isBuyerMaker'], trade['isBestMatch'], trade['symbol']])
        conn.commit()
    cur.execute("""insert into assets_prices(price, symbol, time) values(%s, %s, %s)""",
                [float(prices['price']), prices['symbol'], int(time_r)])
    conn.commit()
    conn.close()


def get_data():
    pairs = ["ETHUSDT", "BTCUSDT", "DOGEUSDT"]
    while True:
        time_start = time.time()
        threads = []
        for pair in pairs:
            t = threading.Thread(target=insert_symbol, args=(pair,))
            threads.append(t)
            t.start()
        for t in threads:
            t.join()
        time_end = time.time()
        try:
            time.sleep(TIME_GAP - (time_end - time_start))
        except ValueError:
            pass


if __name__ == '__main__':
    create_DB()
    get_data()
