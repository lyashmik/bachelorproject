import sys

import requests
import time
import psycopg2
import threading

TIME_GAP = 30.0


def create_DB():
    conn = psycopg2.connect(
        host='localhost',
        database='orderbooks5',
        user='lyashmik',
        password='postgres')
    cur = conn.cursor()

    # get the count of tables with the name
    cur.execute("select exists(select * from information_schema.tables where table_name=%s)", ('order_books',))

    # if the count is 1, then table exists
    if cur.fetchone()[0]:
        return

    cur.execute('CREATE TABLE order_books (lastUpdateId BIGINT, price FLOAT, VELOCITY FLOAT, side VARCHAR,'
                'time BIGINT, symbol VARCHAR, level INTEGER)')
    conn.commit()
    conn.close()


def add_level(arr):
    for index, item in enumerate(arr):
        arr[index].append(index + 1)
    return arr


def insert_symbol(symbol):
    conn = psycopg2.connect(
        host='localhost',
        database='orderbooks5',
        user='lyashmik',
        password='postgres')

    time_r = time.time()
    r = requests.get("https://api.binance.com/api/v3/depth",
                     params=dict(symbol=symbol))
    order_book = r.json()

    cur = conn.cursor()
    bids = add_level(order_book['bids'])
    asks = add_level(order_book['asks'])

    for index, item in enumerate(bids):
        sql = """insert into order_books(lastUpdateId, price, VELOCITY, side, time, symbol, level) values(%s, %s, %s, %s, %s, %s, %s)"""
        cur.execute(sql,
                    [order_book['lastUpdateId'], item[0], item[1], 'bid', int(time_r), symbol, item[2]])
        cur.execute(sql,
                    [order_book['lastUpdateId'], asks[index][0], asks[index][1], 'ask', int(time_r), symbol, asks[index][2]])
    conn.commit()
    conn.close()


def get_data():
    pairs = ["ETHUSDT", "BTCUSDT", "DOGEUSDT"]
    while True:
        time_start = time.time()
        threads = []
        for pair in pairs:
            t = threading.Thread(target=insert_symbol, args=(pair,))
            threads.append(t)
            t.start()
        for t in threads:
            t.join()
        time_end = time.time()
        try:
            time.sleep(TIME_GAP - (time_end - time_start))
        except ValueError:
            pass


if __name__ == '__main__':
    create_DB()
    get_data()
